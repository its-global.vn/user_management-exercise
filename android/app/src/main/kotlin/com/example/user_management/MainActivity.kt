package com.example.user_management

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.TextUtils
import android.util.Base64
import android.util.Log
import androidx.annotation.RequiresApi
import com.orhanobut.logger.Logger
import com.zing.zalo.zalosdk.oauth.LoginVia
import com.zing.zalo.zalosdk.oauth.OAuthCompleteListener
import com.zing.zalo.zalosdk.oauth.OauthResponse
import com.zing.zalo.zalosdk.oauth.ZaloSDK
import com.zing.zalo.zalosdk.oauth.model.ErrorResponse
import io.flutter.embedding.android.FlutterActivity
import io.flutter.embedding.engine.FlutterEngine
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import org.json.JSONObject
import vn.momo.momo_partner.AppMoMoLib
import vn.zalopay.sdk.ZaloPayError
import vn.zalopay.sdk.ZaloPaySDK
import vn.zalopay.sdk.listeners.PayOrderListener
import java.nio.charset.StandardCharsets
import java.security.KeyFactory
import java.security.MessageDigest
import java.security.SecureRandom
import java.security.spec.X509EncodedKeySpec
import java.util.Base64.getDecoder
import java.util.Base64.getEncoder
import javax.crypto.Cipher

class MainActivity: FlutterActivity() {
    private val appID: Int = 554
    private var pendingResult: MethodChannel.Result? = null
    private var orderId: String? = null
    private val amount = 1000
    private val fee = "0"
//    var environment = 0 //developer default

    private val merchantName = "ShopApp"
    private val merchantCode = "MOMOSWBC20220817"
    private val description = "Demo Thanh toán Momo"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        ZaloPaySDK.init(appID, vn.zalopay.sdk.Environment.SANDBOX)
        AppMoMoLib.getInstance().setEnvironment(AppMoMoLib.ENVIRONMENT.DEVELOPMENT)
    }
    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        Log.d("newIntent", intent.toString())
        ZaloPaySDK.getInstance().onResult(intent)
        println(intent.toString())
    }

    private val channelPayOrder: String = "com.example.flutter_zalopay/payOrder"

    private val channelName = "com.example.test_login/zalo"

    private val chanelMomoName= "com.example.flutter_momo/payment"
    @RequiresApi(Build.VERSION_CODES.O)
    override fun configureFlutterEngine(flutterEngine: FlutterEngine) {
        super.configureFlutterEngine(flutterEngine)

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger, channelPayOrder
        ).setMethodCallHandler { call, result ->
            when (call.method) {
                "payOrder" -> {
                    payOrder(call, result)
                }
                else -> {
                    result.notImplemented()
                }
            }
        }

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            channelName
        ).setMethodCallHandler { call, result ->
            try {
                when (call.method) {
                    "login" -> {
                        loginZalo(result)
                    }
                    "logout" -> {
                        logOut(result)
                    }
                    "getProfile" -> {
                        getProfile(result)
                    }
                    else -> {
                        result.notImplemented()
                    }
                }
            } catch (e: Exception) {
                result.success(e.toString())
            }
        }

        MethodChannel(
            flutterEngine.dartExecutor.binaryMessenger,
            chanelMomoName
        ).setMethodCallHandler { call, result->
            try {
                when (call.method) {
                    "request_payment" -> {
                        requestPayment(result)
                    }
                    "encryptRSA" -> {
                        encryptDataByRSA(result)
                    }
                    else -> {
                        result.notImplemented()
                    }
                }
            } catch (e: Exception) {
                result.success(e.toString())
            }
        }
    }
    private fun getProfile(result: MethodChannel.Result) {
        val fields = arrayOf("id", "birthday", "gender", "picture", "name")
        val accessToken = ""

        ZaloSDK.Instance.getAccessTokenByOAuthCode(
            this, accessToken, "",
        ) {
        }

        ZaloSDK.Instance.getProfile(
            this, accessToken,
            { jsonObject: JSONObject? ->
                if (jsonObject == null) {
                    result.success(null)
                } else {
                    result.success(ZaloSDK.Instance.zaloDisplayname)
                }
            },
            fields,
        )
    }

    private fun logOut(result: MethodChannel.Result) {
        ZaloSDK.Instance.unauthenticate()
        result.success(null)
    }

    private fun loginZalo(result: MethodChannel.Result) {
        val verifyCode = generateCodeVerifier()
        val challengeCode = generateCodeChallenge(verifyCode)

        val listener = object : OAuthCompleteListener() {
            override fun onGetOAuthComplete(response: OauthResponse?) {
                if (TextUtils.isEmpty(response?.oauthCode)) {
                    println("❌❌❌❌❌❌❌❌❌❌❌ ERROR: ${response?.errorCode} ${response?.errorMessage}")
                } else {
                    println("✅✅✅✅✅✅✅✅✅✅✅ LOGIN SUCCESS")

                    val map: MutableMap<String, Any?> = HashMap()
                    map["isSuccess"] = true
                    map["oauthCode"] = response?.oauthCode
                    map["refreshToken"] = response?.refreshToken
                    result.success(map)
                }
            }

            override fun onAuthenError(p0: ErrorResponse?) {
                println("❌❌❌❌❌❌❌❌❌❌❌ ERROR: ${p0?.errorCode} ${p0?.errorMsg}")

                val map: MutableMap<String, Any?> = HashMap()
                map["isSuccess"] = false
                map["errorCode"] = p0?.errorCode
                map["errorMsg"] = p0?.errorMsg
                result.success(map)
            }
        }

        ZaloSDK.Instance.authenticateZaloWithAuthenType(
            this,
            LoginVia.APP_OR_WEB,
            challengeCode,
            listener
        )
    }

    private fun generateCodeVerifier(): String? {
        return try {
            val secureRandom = SecureRandom()
            val codeVerifier = ByteArray(32)
            secureRandom.nextBytes(codeVerifier)
            val encoded = Base64.encodeToString(codeVerifier, Base64.DEFAULT)
            trimSpecialChars(encoded)
        } catch (e: Exception) {
            null
        }
    }

    private fun trimSpecialChars(str: String): String {
        return str.trim().replace("+", "-")
            .replace("/", "_").replace("=", "").replace("\\n", "")
    }

    private fun generateCodeChallenge(verifyCode: String?): String? {
        return try {
            val bytes = verifyCode?.toByteArray(charset("US-ASCII"))
            val messageDigest = MessageDigest.getInstance("SHA-256")
            messageDigest.update(bytes!!, 0, bytes.size)
            val digest = messageDigest.digest()
            val encoded = Base64.encodeToString(digest, Base64.DEFAULT)
            trimSpecialChars(encoded)
        } catch (e: Exception) {
            null
        }
    }


    private fun payOrder(call: MethodCall, result: MethodChannel.Result) {
        try {
            val token = call.argument<String>("zptoken")
            val merchantAppDeeplink = "demozpdk://app"

            ZaloPaySDK.getInstance()
                .payOrder(this, token!!, merchantAppDeeplink, object : PayOrderListener {
                    override fun onPaymentCanceled(zpTransToken: String?, appTransID: String?) {
                        //Handle User Canceled
                        result.success("❌❌❌ User Canceled")
                    }

                    override fun onPaymentError(
                        zaloPayErrorCode: ZaloPayError?,
                        zpTransToken: String?,
                        appTransID: String?,
                    ) {
                        //Redirect to Zalo/ZaloPay Store when zaloPayError == ZaloPayError.PAYMENT_APP_NOT_FOUND
                        //Handle Error
                        result.success("❌❌❌ Payment failed")
                    }

                    override fun onPaymentSucceeded(
                        transactionId: String,
                        transToken: String,
                        appTransID: String?,
                    ) {
//                        //Handle Success
//                        result.success("✅✅✅✅ Payment Success")
                    }
                })
        } catch (e: Exception) {
            result.success("❌❌❌ Payment failed: ${e.message}")
        }

    }

    override fun onActivityResult(requestCode:Int, resultCode:Int, data: Intent) {
        super.onActivityResult(requestCode, resultCode, data)
      if(requestCode==1000){
          if (requestCode == AppMoMoLib.getInstance().REQUEST_CODE_MOMO && resultCode == -1){
              if (data.getIntExtra("status", -1) == 0){
                  //TOKEN IS AVAILABLE
                  val token: String? = data.getStringExtra("data") // Token response

                  val phoneNumber: String? = data.getStringExtra("phonenumber")
                  val env: String? = data.getStringExtra("env")
                  Logger.i("Token: $token\nphoneNumber: $phoneNumber\nenv: $env")
                  if (token != null && token != ""){
                      // TODO: send phoneNumber & token to your server side to process payment with MoMo server
                      // IF Momo topup success, continue to process your order
                      val returnValue: MutableMap<String?, Any?> = java.util.HashMap<String?, Any?>()
                      returnValue["token"] = token
                      returnValue["phonenumber"] = phoneNumber
                      returnValue["orderId"] = orderId
                      pendingResult!!.success(returnValue)
                  } else {
                      onPaymentFailed()
                  }
              } else if (data.getIntExtra("status", -1) == 1){
                  //TOKEN FAIL
                  onPaymentFailed()
              } else if (data.getIntExtra("status", -1) == 2){
                  //TOKEN FAIL
                  onPaymentFailed()
              } else {
                  //TOKEN FAIL
                  onPaymentFailed()
              }
          } else {
              onPaymentFailed()
          }
      }
        else{
          ZaloSDK.Instance.onActivityResult(this, requestCode, resultCode, data) // <-- Add this line
        }

    }

    private fun requestPayment(result: MethodChannel.Result) {
        pendingResult = result
        orderId = "MM" + System.currentTimeMillis()
        AppMoMoLib.getInstance().setAction(AppMoMoLib.ACTION.PAYMENT)
        AppMoMoLib.getInstance().setActionType(AppMoMoLib.ACTION_TYPE.GET_TOKEN)

        // https://developers.momo.vn/v2/#/docs/app_in_app?id=tham-s%e1%bb%91-t%e1%ba%a1o-deeplink-m%e1%bb%9f-app-momo
        val eventValue: MutableMap<String, Any> = java.util.HashMap()
        // client Required
        eventValue["merchantname"] =
            merchantName //Tên đối tác. được đăng ký tại https://business.momo.vn. VD: Google, Apple, Tiki , CGV Cinemas
        eventValue["merchantcode"] =
            merchantCode //Mã đối tác, được cung cấp bởi MoMo tại https://business.momo.vn
        eventValue["amount"] = amount // Kiểu integer
        eventValue["orderId"] =
            orderId!! //uniqueue id cho Bill order, giá trị duy nhất cho mỗi đơn hàng
        eventValue["orderLabel"] = "Mã đơn hàng" //gán nhãn

        // client Optional - bill info
        eventValue["merchantnamelabel"] = "Dịch vụ" //gán nhãn
        eventValue["fee"] = fee //Kiểu integer
        eventValue["description"] = description //mô tả đơn hàng - short description

        // client extra data
        eventValue["requestId"] = merchantCode + "merchant_billId_" + System.currentTimeMillis()
        eventValue["partnerCode"] = merchantCode
        eventValue["extra"] = ""
        AppMoMoLib.getInstance().requestMoMoCallBack(this, eventValue)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    @Throws(java.lang.Exception::class)
    private fun encryptDataByRSA(result: MethodChannel.Result) {
        try {
            val data = JSONObject()
            data.put("partnerCode", "MOMOSWBC20220817")
            data.put("partnerRefId", orderId)
            data.put("amount", amount)
            Logger.i(data.toString())
            val publicKey =
                "MIICIjANBgkqhkiG9w0BAQEFAAOCAg8AMIICCgKCAgEAlKWmle6myh1b6BEW0ZtSIZbEyPpp5EVDmLbG6zkbAjQCBZM7tszKr7HNVtuLDa3X16Mawr2QmntVkC/ntIwH+syW78mRUAIxEiBmWwbYf8Ya2+ux+1d4+iGRO9SbD9LuhHoqfABdK1NWCtHrCDJ/IN+YKU2Jb06E0WUvug+WmGJcK507V29tpn5ga09FDfok1FzB55VfTqRwhEdbE191PpfplvtN3ymVwOGmDEBxTTDbpndzu/NcASjpAt5VoZs6eB8VBfANwLeAfx+QG/wjjmQoxNfdWKALFpP0GNHXyIkIniy8x/JdkZINt5wnEtmjD8EhuvJGB3Asj0C7VK2RkuDamlOHoOKJn+wFHYmPjgSJfzN66VotWUNKPUKQVb9RcfR9rmUsaQkfP2tVMXJqxdyj2U03fre6tLUznS7cJaWvk+JTL0DMgdbX+jRE4qQBKF5pat6jTIuT190eYsNRVI8VMjOpkQsNOJcAnK0a3yfWntbakkVojowklFkMkxIjgAl9+a0/xCoLjFg14npsESsus3nikEid2CRp7dGD/JRskVipnCXd3ILi4+h11u2U1OXWBDUDho0B0XNiVmoApdEpP3AIBGc993HOyKZ5qfseEQnbXzwcOOdOd7I1Nw4qVugNXTDOb7eSTkGP1vRj0/pTrfNItjdfIi68TnC1iHcCAwEAAQ=="
            val publicKeyBytes = getDecoder().decode(publicKey)
            val keySpec = X509EncodedKeySpec(publicKeyBytes)
            val keyFactory = KeyFactory.getInstance("RSA")
            val rsaPublicKey = keyFactory.generatePublic(keySpec)
            val cipher = Cipher.getInstance("RSA/ECB/PKCS1Padding")
            cipher.init(Cipher.ENCRYPT_MODE, rsaPublicKey)
            val encryptedData = cipher.doFinal(data.toString().toByteArray(StandardCharsets.UTF_8))
            val dataEncrypted = getEncoder().encodeToString(encryptedData)
            result.success(dataEncrypted)
        } catch (e: Exception) {
            result.success(null)
        }
    }
    private fun onPaymentFailed() {
        val notReceiveInfoMessage = "message: Không nhận được thông tin"
        Logger.wtf(notReceiveInfoMessage)
        this.pendingResult!!.success(null)
    }
}
