import 'package:firebase_storage/firebase_storage.dart';
import 'dart:io';
class FireBaseData {
  Future<String> pushAndGetLinkDownloadFileFromStorage(File file) async {
    final storageRef = FirebaseStorage.instance.ref();
    final Reference mountainsRef = storageRef.child(file.path);
    await mountainsRef.putFile(file);
    final fileUrl = await mountainsRef.getDownloadURL();
    return fileUrl;
  }
}
