import 'dart:async';
import 'dart:convert';
import 'package:path_provider/path_provider.dart';
import 'dart:io';
import 'package:user_management/model/user_model.dart';
import 'package:xml/xml.dart';

class LocalStorage {
  Future<String> get _localPath async {
    final directory = await getApplicationDocumentsDirectory();
    return directory.path;
  }

  Future<File> get _localJsonFile async {
    final path = await _localPath;
    return File('$path/user.json');
  }

  Future<File> get _localXMLFile async {
    final path = await _localPath;
    return File('$path/user.xml');
  }

  Future<List<UserModel>> readJson() async {
    try {
      final file = await _localJsonFile;
      if(!file.existsSync()) file.createSync(recursive: true);
      final data = await file.readAsString();
      final List list = jsonDecode(data);
      return list.map((e) => UserModel.fromJson(e)).toList();
    } catch (e) {
      return [];
    }
  }

  Future<void> writeJson(List<UserModel> list) async {
    final file = await _localJsonFile;
    await file.writeAsString(jsonEncode(list));
  }

  Future<List<UserModel>> readXml() async {
    late List<UserModel> list = [];
     final File file = await _localXMLFile;
     if(!file.existsSync()) file.createSync(recursive: true);
     final XmlDocument data = XmlDocument.parse(file.readAsStringSync());
     final allElements = data.findAllElements('user');
     for (var e in allElements) {
       list.add(UserModel(
         id: e.getElement('id')!.text,
         username: e.getElement('username')!.text,
         password: e.getElement('password')!.text,
         email: e.getElement('email')!.text,
         typeFile: e.getElement('typeFile')!.text,
       ));
     }
    return list;
  }

  Future<void> writeXml(List<UserModel> list) async {
    try {
      final file = await _localXMLFile;
      final builder = XmlBuilder();
      builder.processing('xml', 'version="1.0"');
      builder.element('listuser', nest: () {
        for (var e in list) {
          builder.element('user', nest: () {
            builder.element('id', nest: e.id);
            builder.element('username', nest: e.username);
            builder.element('password', nest: e.password);
            builder.element('email', nest: e.email);
            builder.element('typeFile', nest: e.typeFile);
          });
        }
      });
      final document = builder.buildDocument();
      file.writeAsStringSync(document.toXmlString(pretty: true));
    } catch (e) {
      rethrow;
    }
  }

  Future<List<UserModel>> readAllUserData() async {
    List<UserModel> listJsonData = await readJson();
    List<UserModel> listXmlData = await readXml();
    return listJsonData + listXmlData;
  }

  Future<void> deleteUser(UserModel userModel) async {
    List<UserModel> listXmlUser = await LocalStorage().readXml();
    List<UserModel> listJsonUser = await LocalStorage().readJson();
    if (userModel.typeFile == "json") {
      listJsonUser.removeWhere(
            (element) => element.id == userModel.id,
      );
      LocalStorage().writeJson(listJsonUser);
    }
    if (userModel.typeFile == "xml") {
      listXmlUser.removeWhere(
            (element) => element.id == userModel.id,
      );
      LocalStorage().writeXml(listXmlUser);
    }
  }

  Future<void> updateUser(UserModel userModel) async {
    List<UserModel> listXmlUser = await LocalStorage().readXml();
    List<UserModel> listJsonUser = await LocalStorage().readJson();
    if (userModel.typeFile == 'json') {
      listJsonUser[int.parse(userModel.id)] = userModel;
      LocalStorage().writeJson(listJsonUser);
    } else {
      listXmlUser[int.parse(userModel.id)] = userModel;
      LocalStorage().writeXml(listXmlUser);
    }
  }
}
