import 'dart:convert';
import 'dart:io';
import 'package:path_provider/path_provider.dart';
import 'package:user_management/model/user_model.dart';

class ExportData{
      Future<String> get _localPath async {
        final directory = await getApplicationDocumentsDirectory();
        return directory.path;
      }

    Future<String> getExportFilePath() async {
      final path = await _localPath;
      return "$path/export-data.json";
    }

    Future<void> addToExportFile(List<UserModel> list) async {
      final path = await _localPath;
      final File file = File("$path/export-data.json");
      await file.writeAsString(jsonEncode(list));
    }
}