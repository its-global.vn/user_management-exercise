import 'dart:convert';
import 'dart:io';
import 'package:file_picker/file_picker.dart';
import 'package:user_management/model/user_model.dart';
import 'local_data.dart';

class ImportData{
  Future<List<UserModel>> readFile() async {
    FilePickerResult? result = await FilePicker.platform.pickFiles();
    File file = File(result!.files.single.path!);
    if (!file.existsSync()) file.createSync(recursive: true);
    final data = await file.readAsString();
    final List listReadFromJson = jsonDecode(data);
    final List<UserModel> list = listReadFromJson
        .map((e) => UserModel.fromJson(e))
        .toList();
    return list;
  }

  Future<void> writeToXmlAndJson(List<UserModel> list) async{
    for (var e in list) {
      if (e.typeFile == 'json'){
        List<UserModel> listJson = await  LocalStorage().readJson();
        listJson.add(e);
        LocalStorage().writeJson(listJson);
      }
      if (e.typeFile == 'xml') {
        List<UserModel> listXml = await  LocalStorage().readXml();
        listXml.add(e);
        LocalStorage().writeJson(listXml);
      }
    }
  }
}