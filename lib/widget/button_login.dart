import 'package:flutter/material.dart';
class LoginWithButton extends StatelessWidget {
    final IconData iconData;
    final String tittle;
    final GestureTapCallback? onTap;
    const LoginWithButton({Key? key, required this.iconData, required this.tittle, this.onTap }) : super(key: key);

  @override
  Widget build(BuildContext context) {
      return Center(
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            onTap: onTap,
            child: Container(
              height: 50,
              width: 300,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black),
                borderRadius: const BorderRadius.all(Radius.circular(8.0)),
              ),
              child: Row(
                children: [
                  const SizedBox(
                    width: 30,
                  ),
                  Icon(iconData,size: 28,),
                  const SizedBox(
                    width: 40,
                  ),
                  Text(" $tittle",
                    style:
                    const TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 18,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
  }
}
