import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:user_management/data/stored_data/local_data.dart';
import 'package:user_management/model/user_model.dart';
class AddUserForm extends StatefulWidget {
  const AddUserForm({Key? key}) : super(key: key);
  @override
  State<AddUserForm> createState() => _AddUserFormState();
}

class _AddUserFormState extends State<AddUserForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  late SharedPreferences prefs;
  Future initData() async {
    prefs = await _prefs;
    if(prefs.getInt('incrementID') == null) prefs.setInt('incrementID', 0);
  }
  @override
  void initState() {
    // TODO: implement initState
    initData();
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      width: double.maxFinite,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: TextFormField(
                controller: usernameController,
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Username'),
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Username cant be empty';
                  }
                  return null;
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Password'),
                controller: passwordController,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Password cant be empty';
                  }
                  return null;
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Email'),
                controller: emailController,
                validator: (value) {
                  if (!EmailValidator.validate(value!)) {
                    return 'Invalid Email';
                  }
                  return null;
                },
              ),
            ),
            const SizedBox(
              height: 40,
            ),
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all(Colors.white),
                      backgroundColor:
                      MaterialStateProperty.all(Colors.green),
                    ),
                    onPressed: () async {
                      saveData("xml");
                    },
                    child: const Text('Save in XML'),
                  ),
                  const Spacer(),
                  TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all(Colors.white),
                      backgroundColor:
                      MaterialStateProperty.all(Colors.green),
                    ),
                    onPressed: () async {
                      saveData("json");
                    },
                    child: const Text('Save in Json'),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
  Future<void> saveData(String typeFile) async {
    prefs.setInt('incrementID',
        prefs.getInt('incrementID')! + 1);
    if (_formKey.currentState!.validate()) {
      UserModel userModel  = UserModel(
          id: prefs.getInt('incrementID')!.toString(),
          username: usernameController.text,
          password: passwordController.text,
          email: emailController.text,
          typeFile: typeFile);
      if(typeFile == "json"){
        List<UserModel> listJsonUser = await LocalStorage().readJson();
        listJsonUser.add(userModel);
        LocalStorage().writeJson(listJsonUser);
      }
      if(typeFile == "xml"){
        List<UserModel> listXmlUser = await LocalStorage().readXml();
        listXmlUser.add(userModel);
        LocalStorage().writeXml(listXmlUser);
      }
      clear();
      if (context.mounted) Navigator.pop(context);
      setState(() {

      });
    }
  }
  void clear(){
    usernameController.clear();
    passwordController.clear();
    emailController.clear();
  }
}
