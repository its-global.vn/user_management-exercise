import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:user_management/data/stored_data/local_data.dart';
import 'package:user_management/model/user_model.dart';
class ChangeUserForm extends StatefulWidget {
  final UserModel userModel;
  const ChangeUserForm({Key? key, required this.userModel}) : super(key: key);

  @override
  State<ChangeUserForm> createState() => _ChangeUserFormState();
}

class _ChangeUserFormState extends State<ChangeUserForm> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    final String getTypeFile = widget.userModel.typeFile;
    return SizedBox(
      height: 300,
      width: double.maxFinite,
      child: Form(
        key: _formKey,
        child: Column(
          children: [
            Expanded(
              flex: 1,
              child: TextFormField(
                initialValue: widget.userModel.username,
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Username'),
                onChanged: (value) => widget.userModel.username = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Username cant be empty';
                  }
                  return null;
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Password'),
                initialValue: widget.userModel.password,
                onChanged: (value) => widget.userModel.password = value,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return 'Password cant be empty';
                  }
                  return null;
                },
              ),
            ),
            Expanded(
              flex: 1,
              child: TextFormField(
                keyboardType: TextInputType.multiline,
                maxLength: null,
                maxLines: 2,
                decoration: const InputDecoration(hintText: 'Email'),
                initialValue: widget.userModel.email,
                onChanged: (value) => widget.userModel.email = value,
                validator: (value) {
                  if (!EmailValidator.validate(value!)) {
                    return 'Invalid Email';
                  }
                  return null;
                },
              ),
            ),
            Expanded(
                flex: 1,
                child: Center(child: Text("Type File: .$getTypeFile"))),
            Expanded(
              flex: 1,
              child: Row(
                children: [
                  TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all(Colors.white),
                      backgroundColor:
                      MaterialStateProperty.all(Colors.red),
                    ),
                    onPressed: () async {
                      await LocalStorage().deleteUser(widget.userModel);
                      if (context.mounted) Navigator.pop(context);
                    },
                    child: const Text('Delete User'),
                  ),
                  const Spacer(),
                  TextButton(
                    style: ButtonStyle(
                      foregroundColor:
                      MaterialStateProperty.all(Colors.white),
                      backgroundColor:
                      MaterialStateProperty.all(Colors.blue),
                    ),
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        LocalStorage().updateUser(widget.userModel);
                        if (context.mounted) Navigator.pop(context);
                      }
                    },
                    child: const Text('Save your change'),
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
