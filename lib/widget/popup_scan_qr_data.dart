import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:url_launcher/url_launcher_string.dart';
import 'package:user_management/screen/list_user_screen.dart';
class ScanQRData extends StatefulWidget {
  final String? data;
  const ScanQRData({Key? key, required this.data}) : super(key: key);

  @override
  State<ScanQRData> createState() => _ScanQRDataState();
}

class _ScanQRDataState extends State<ScanQRData> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 100,
      width: double.maxFinite,
      child: Column(
        children: [
          Row(
            children:  [
              const Padding(
                padding: EdgeInsets.only(right: 25.0),
                child: Icon(Icons.link),
              ),
              Flexible(
                child: Text(
                    overflow: TextOverflow.ellipsis,
                    style: const TextStyle(fontWeight: FontWeight.bold,fontSize: 15),
                    maxLines: 2,
                    widget.data ?? "",
                ),
              )
            ],
          ),
          Padding(
            padding: const EdgeInsets.only(top:15.0),
            child: Center(
              child: TextButton(
                style: ButtonStyle(
                  foregroundColor:
                  MaterialStateProperty.all(Colors.white),
                  backgroundColor:
                  MaterialStateProperty.all(Colors.blue),
                ),
                onPressed: () async {
                  await launchUrlString(widget.data ?? " ",mode: LaunchMode.externalApplication);
                  if(context.mounted )Navigator.pushReplacement(context,PageTransition(child: const ListUserScreen(), type: PageTransitionType.fade));
                },
                child: const Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(Icons.language),
                    Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Text("Open link"),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
