import 'dart:developer';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:logger/logger.dart';

class Auth {
  static const channel = MethodChannel("com.example.test_login/zalo");
  Future<void> loginWithFacebook() async {
    final facebookLoginResult = await FacebookAuth.instance.login();
    final userData = await FacebookAuth.instance.getUserData();
  }

  Future<void> logoutFacebook(VoidCallback onCallback) async {
    try {
      await FacebookAuth.instance.logOut();
      onCallback.call();
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> loginWithGoogle() async{
    // GoogleSignIn(scopes: ['email']);
    final googleSignIn = GoogleSignIn(scopes: ['email']);
    final googleSignInAccount = await googleSignIn.signIn();

  }

  Future<void> logoutGoogle(VoidCallback onCallback) async {
    try {
      await FirebaseAuth.instance.signOut();
      onCallback.call();
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> loginWithZalo (VoidCallback onCallback) async {
    try {
      final Map<dynamic, dynamic>? data = await channel.invokeMethod('login');
      final logger = Logger();
      if (data != null && data["isSuccess"]) {
        logger.i(data.toString());
        onCallback.call();
      }
    } catch (e) {
      log(e.toString());
    }
  }

  Future<void> logoutZalo(VoidCallback onCallback) async {
    try {
      await channel.invokeMethod('logout');
      onCallback.call();
    } catch (e) {
      log(e.toString());
    }
  }
}
