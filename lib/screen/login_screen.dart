import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'package:user_management/authention/authention.dart';
import 'package:user_management/screen/list_user_screen.dart';
import 'package:user_management/screen/zalo_screen.dart';
import 'package:user_management/screen/zalo_pay_screen.dart';
import '../widget/button_login.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("AppBar"),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          LoginWithButton(
            iconData: Icons.facebook_rounded,
            tittle: "Login with Facebook",
            onTap: () async {
              await Auth().loginWithFacebook();
            },
          ),
          LoginWithButton(
            iconData: Icons.language,
            tittle: "Login with Google",
            onTap: () async {
              await Auth().loginWithGoogle();
            },
          ),
          LoginWithButton(
            iconData: Icons.arrow_right_alt_outlined,
            tittle: "Into List User Screen",
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      child: const ListUserScreen(),
                      type: PageTransitionType.rightToLeft));
            },
          ),
          LoginWithButton(
            iconData: Icons.monetization_on_rounded,
            tittle: "Pay with Zalo Pay",
            onTap: () {
              Navigator.push(
                  context,
                  PageTransition(
                      child: const ZaloPayScreen(),
                      type: PageTransitionType.rightToLeft));
            },
          ),
          LoginWithButton(
            iconData: Icons.comment,
            tittle: "Login with zalo",
            onTap: () async {
              await Auth().loginWithZalo(() {
                Navigator.push(
                    context,
                    PageTransition(
                        child: const ZaloScreen(),
                        type: PageTransitionType.rightToLeft));
              });
            },
          ),
        ],
      ),
    );
  }
}
