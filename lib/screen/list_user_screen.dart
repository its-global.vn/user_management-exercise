import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:page_transition/page_transition.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:user_management/data/api/api_firebase.dart';
import 'package:user_management/data/stored_data/export_data.dart';
import 'package:user_management/data/stored_data/import_data.dart';
import 'package:user_management/data/stored_data/local_data.dart';
import 'package:user_management/model/user_model.dart';
import 'dart:io';
import 'package:share_plus/share_plus.dart';
import 'package:cross_file/cross_file.dart';
import 'package:user_management/widget/add_user_form.dart';
import 'package:user_management/widget/change_user_form.dart';
import 'package:user_management/screen/qr_code_scanner_screen.dart';

class ListUserScreen extends StatefulWidget {
  const ListUserScreen({Key? key}) : super(key: key);

  @override
  State<ListUserScreen> createState() => _ListUserScreenState();
}

class _ListUserScreenState extends State<ListUserScreen> {
  List<UserModel> list = [];
  List<UserModel> listXmlUser = [];
  void initData() async {
    await LocalStorage().writeXml([]);
    listXmlUser = await LocalStorage().readXml();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    initData();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: Padding(
          padding: const EdgeInsets.all(8.0),
          child: GestureDetector(
            child: const Icon(Icons.person_add_alt_rounded),
            onTap: () {
              showDialog(
                context: context,
                builder: (context) => const AlertDialog(
                  title: Center(child: Text("Add New User")),
                  content: AddUserForm(),
                ),
              ).then((value) {
                setState(() {});
              });
            },
          ),
        ),
        title: const Center(child: Text("List User")),
        actions: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: GestureDetector(
              onTap: () => Navigator.push(
                  context,
                  PageTransition(
                      type: PageTransitionType.rightToLeft,
                      child: const QrCodeScannerPage())),
              child: const Icon(Icons.qr_code),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: PopupMenuButton(
              itemBuilder: (context) => [
                PopupMenuItem(
                  onTap: () async {
                    final List<UserModel> listImportData =
                        await ImportData().readFile();
                    ImportData().writeToXmlAndJson(listImportData);
                    list = await LocalStorage().readAllUserData();
                    setState(() {});
                  },
                  child: const Text("Import Data"),
                ),
                PopupMenuItem(
                  child: const Text("Export Data"),
                  onTap: () async {
                    list = await LocalStorage().readAllUserData();
                    ExportData().addToExportFile(list);
                    String filePath = await ExportData().getExportFilePath();
                    Share.shareXFiles([XFile(filePath)]);
                  },
                ),
              ],
              child: const Icon(Icons.more_vert),
            ),
          ),
        ],
      ),
      body: Container(
        color: Colors.white54,
        child: FutureBuilder(
          future: LocalStorage().readAllUserData(),
          builder: (context, snapshot) {
            return snapshot.hasData
                ? ListView.builder(
                    itemCount: snapshot.data!.length,
                    itemBuilder: (context, index) => Card(
                          color: Colors.blue.withOpacity(0.25),
                          child: Slidable(
                            startActionPane: ActionPane(
                              motion: const DrawerMotion(),
                              children: [
                                SlidableAction(
                                  flex: 2,
                                  backgroundColor: const Color(0xFF0392CF),
                                  foregroundColor: Colors.white,
                                  icon: Icons.edit_outlined,
                                  label: 'Change',
                                  onPressed: (context) {
                                    showDialog(
                                      context: context,
                                      builder: (context) => AlertDialog(
                                        title: const Center(
                                            child: Text(
                                                "Change User's Information")),
                                        content: ChangeUserForm(
                                          userModel: snapshot.data![index],
                                        ),
                                      ),
                                    ).then((value) {
                                      setState(() {});
                                    });
                                  },
                                ),
                                SlidableAction(
                                  flex: 2,
                                  backgroundColor: const Color(0xFF21B7CA),
                                  foregroundColor: Colors.white,
                                  icon: Icons.share,
                                  label: 'Share',
                                  onPressed: (context) async {
                                    final String getPath =
                                        await ExportData().getExportFilePath();
                                    await ExportData().addToExportFile(
                                        [snapshot.data![index]]);
                                    String dataUrl = await FireBaseData()
                                        .pushAndGetLinkDownloadFileFromStorage(
                                            File(getPath));
                                    _showQRCode(dataUrl);
                                  },
                                ),
                              ],
                            ),
                            child: ListTile(
                              title: Text(
                                  snapshot.data![index].username.toString()),
                              subtitle:
                                  Text(snapshot.data![index].email.toString()),
                              leading:
                                  Text(".${snapshot.data![index].typeFile}"),
                            ),
                          ),
                        ))
                : const Center(child: Text("Empty Data"));
          },
        ),
      ),
    );
  }

  Future _showQRCode(String dataUrl) {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: const Center(child: Text("QR Code")),
              content: SizedBox(
                height: 250,
                child: Column(
                  children: [
                    SizedBox(
                        height: 200,
                        width: 200,
                        child: QrImageView(
                          data: dataUrl,
                          size: 310,
                        )),
                    const Spacer(),
                    TextButton(
                      style: ButtonStyle(
                        foregroundColor:
                            MaterialStateProperty.all(Colors.white),
                        backgroundColor:
                            MaterialStateProperty.all(Colors.green),
                      ),
                      onPressed: () => Navigator.pop(context),
                      child: const Text('Ok'),
                    ),
                  ],
                ),
              ),
            ));
  }
}
