import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';
import 'package:user_management/widget/popup_scan_qr_data.dart';

class QrCodeScannerPage extends StatefulWidget {
  const QrCodeScannerPage({Key? key}) : super(key: key);

  @override
  State<QrCodeScannerPage> createState() => _QrCodeScannerPageState();
}

class _QrCodeScannerPageState extends State<QrCodeScannerPage> {
  final GlobalKey qrKey = GlobalKey();
  Barcode? result;
  QRViewController? qrController;
  @override
  void dispose() {
    qrController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: const Center(child: Text("QR scanner")),
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Center(
              child: (result != null)
                  ? Text(
                      'Barcode Type: ${describeEnum(result!.format)}   Data: ${result!.code}')
                  : const Text(
                      'Scan your code here:',
                      style:
                          TextStyle(fontSize: 25, fontWeight: FontWeight.bold),
                    ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: SizedBox(
                height: 400,
                width: double.infinity,
                child: QRView(
                  overlay: QrScannerOverlayShape(),
                  key: qrKey,
                  onQRViewCreated: (value) async {
                    qrController = value;
                    qrController!.scannedDataStream.listen((event) async {
                      await qrController!.pauseCamera();
                      result = event;
                      if (context.mounted) {
                        showDialog(
                          context: context,
                          builder: (context) =>  AlertDialog(
                            title: const Center(child: Text("Data")),
                            content: ScanQRData(data: result!.code),
                          ),
                        );
                      }
                    });
                  },
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
