import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:logger/logger.dart';
import 'package:user_management/screen/login_screen.dart';
import '../authention/authention.dart';
class ZaloScreen extends StatelessWidget {
  const ZaloScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            TextButton(
              onPressed: () {
                _getProfile();
              },
              child: const Text('Get profile'),
            ),
            TextButton(
              onPressed: () {
                Auth().logoutZalo(() {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const LoginScreen(),
                  ));
                });
              },
              child: const Text('Log out Zalo'),
            ),
            TextButton(
              onPressed: () {
                Auth().logoutGoogle(() {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const LoginScreen(),
                  ));
                });
              },
              child: const Text('Log out Google '),
            ),
            TextButton(
              onPressed: () {
                Auth().logoutFacebook(() {
                  Navigator.of(context).pushReplacement(MaterialPageRoute(
                    builder: (context) => const LoginScreen(),
                  ));
                });
              },
              child: const Text('Log out Facebook'),
            ),

          ],
        ),
      ),
    );
  }


  FutureOr<void> _getProfile() async {
    const channel = MethodChannel("com.example.test_login/zalo");
    final data = await channel.invokeMethod('getProfile');
    final logger = Logger();
    logger.i(data);
  }
}
