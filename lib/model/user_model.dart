class UserModel{
  String id;
  String username;
  String email;
  String password;
  String typeFile;



  factory UserModel.fromJson(Map<String, dynamic> json) {
    return UserModel(
      id: json["id"],
      username: json["username"],
      email: json["email"],
      password: json["password"],
      typeFile:json["typeFile"]
    );
  }

  UserModel( {required this.id, required this.username, required this.email, required this.password,required this.typeFile});

  Map<String, dynamic> toJson() {
    return {
      "id": id,
      "username": username,
      "email": email,
      "password": password,
      "typeFile": typeFile,
    };
  }
//
}