import 'package:flutter/material.dart';
import 'package:logger/logger.dart';
import 'package:user_management/model/momo_model/request_model.dart';

import '../model/momo_model/transaction_model.dart';
import '../utils/momo/config.dart';
import '../utils/momo/ultis.dart';

Future<void> _requestPayment() async {

  const channel = MethodChannel('com.example.flutter_momo/payment');

  try {
    final result = await channel.invokeMethod('request_payment');

    Logger().i(result);

    if (result != null) {
      final token = result['token'];
      final phoneNumber = result['phonenumber'];
      final partnerRefId = result['orderId'];
      const partnerCode = MomoConfig.partnerCode;

      final hash = await channel.invokeMethod('encryptRSA');

      Logger().v("hash: $hash");

      final paymentProcessingRequest = PaymentProcessingRequest(
        partnerCode: partnerCode,
        partnerRefId: partnerRefId,
        customerNumber: phoneNumber,
        appData: token,
        hash: hash,
      );

      final paymentProcessingResponse =
      await requestPaymentProcessing(paymentProcessingRequest);
      if (paymentProcessingResponse != null) {
        final requestId = DateTime.now().millisecondsSinceEpoch.toString();
        const requestType = 'capture';
        final momoTransId = paymentProcessingResponse.transid;

        final inputData =
            'partnerCode=$partnerCode&partnerRefId=$partnerRefId&requestType=$requestType&requestId=$requestId&momoTransId=$momoTransId';
        final signature = getSignatureByHmacSHA256(inputData);

        final transactionConfirmRequest = TransactionConfirmRequest(
          partnerCode: partnerCode,
          partnerRefId: partnerRefId,
          requestType: requestType,
          requestId: requestId,
          momoTransId: momoTransId,
          signature: signature,
        );
        final transactionConfirmResponse =
        await requestTransactionConfirm(transactionConfirmRequest);
      }
    }
  } catch (e) {
    Logger().e(e);
  } finally {
  }
}
