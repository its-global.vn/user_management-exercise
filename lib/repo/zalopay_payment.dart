import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:logger/logger.dart';
import 'package:user_management/model/zalo_model/order_model.dart';
import '../model/zalo_model/order_status_model.dart';
import '../screen/zalo_pay_screen.dart';
import '../utils/endpoint.dart';
import '../utils/zalo/config.dart';
import '../utils/zalo/utils.dart' as zalopay_utils;

/// Docs: https://docs.zalopay.vn/v2/general/overview.html#tao-don-hang_thong-tin-don-hang
Future<CreateOrderResponse?> createOrder(int price) async {
  try {
    Map<String, String> headers = {
      "Content-Type": "application/x-www-form-urlencoded",
    };

    appTransId = zalopay_utils.getAppTransId();

    Map<String, String> body = {
      'app_id': ZaloPayConfig.appId.toString(),
      'app_user': ZaloPayConfig.appUser,
      'app_trans_id': appTransId,
      'app_time': DateTime.now().millisecondsSinceEpoch.toString(),
      'amount': price.toStringAsFixed(0),
      'item': "[]",
      'description': zalopay_utils.getDescription(appTransId),
      'embed_data': "{}",
      'bank_code': zalopay_utils.getBankCode(),
    };

    final String hmacInput =
        "${body['app_id']}|${body['app_trans_id']}|${body['app_user']}|${body['amount']}|${body['app_time']}|${body['embed_data']}|${body['item']}";

    body['mac'] = zalopay_utils.getMacCreateOrder(hmacInput);

    Logger().d("body_request: $body");

    final client = http.Client();

    final response = await client.post(
      Uri.parse(ZaloEndpoint.createOrderUrl),
      headers: headers,
      body: body,
    );

    if (response.statusCode != 200) {
      return null;
    }

    final data = jsonDecode(response.body);
    Logger().i("data_response: $data");

    return CreateOrderResponse.fromJson(data);
  } catch (e) {
    rethrow;
  }
}

/// Docs: https://docs.zalopay.vn/v2/general/overview.html#truy-van-trang-thai-thanh-toan-cua-don-hang
Future<OrderStatusResponse?> getOrderStatus(
    {required String appTransId}) async {
  try {
    Map<String, String> headers = {
      "Content-Type": "application/x-www-form-urlencoded",
    };
    final appId = ZaloPayConfig.appId.toString();
    final key1 = ZaloPayConfig.key1.toString();
    final String hmacInput = "$appId|$appTransId|$key1";
    final mac = zalopay_utils.getMacOrderStatus(hmacInput);

    Map<String, String> body = {
      "app_id": ZaloPayConfig.appId.toString(),
      "app_trans_id": appTransId,
      "mac": mac,
    };

    final client = http.Client();

    final response = await client.post(
      Uri.parse(ZaloEndpoint.queryOrderUrl),
      headers: headers,
      body: body,
    );

    if (response.statusCode != 200) {
      return null;
    }

    final data = jsonDecode(response.body);
    Logger().i("order data response: $data");
    return OrderStatusResponse.fromJson(data);
  } catch (e) {
    rethrow;
  }
}
