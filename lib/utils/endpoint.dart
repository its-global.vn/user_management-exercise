// For sandbox
class Endpoint {
  static const createOrderUrl = 'https://sb-openapi.zalopay.vn/v2/create';
  static const queryOrderUrl = 'https://sb-openapi.zalopay.vn/v2/query';
}
